﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace bin2text_Tool
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

                Console.WriteLine(string.Format("No valid parameters.{0}\"{1}\" -h or \"{1}\" --help for usage instructions.", Environment.NewLine, Path.GetFileNameWithoutExtension(fvi.FileName)));

                return;
            }
            else if (args.Length == 2 || args.Length == 3)
            {
                if (!File.Exists(args[0]))
                {
                    Console.WriteLine(string.Format("ERROR: File \"{0}\" does not exist.", args[0], Environment.NewLine));
                    return;
                }

                using (FileStream reader = new FileStream(args[0], FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan))
                using (StreamWriter writer = new StreamWriter(args[1]))
                {
                    byte[] buffer = new byte[args.Length == 2 ? 4096 : int.Parse(args[2])];

                    int readBytes = 0;

                    while ((readBytes = reader.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        if (args.Length == 2)
                            writer.Write(BitConverter.ToString(buffer, 0, readBytes).Replace("-", ""));
                        else
                            writer.WriteLine(BitConverter.ToString(buffer, 0, readBytes).Replace("-", ""));
                    }
                }

                return;
            }

            foreach (string arg in args)
            {
                if (arg == "-v" || arg == "--version")
                {
                    FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

                    Console.WriteLine(Environment.NewLine + "{0} {1} by {2}, written for catalinnc." + Environment.NewLine, fvi.ProductName, fvi.ProductVersion, fvi.CompanyName);
                    return;
                }
                else
                {
                    Console.WriteLine("  -v         --version{0,49}", "Shows the version information.");
                    Console.WriteLine();
                    Console.WriteLine("  inputfile  outputfile  <linelength>{0,41}", "Writes the hexadecimal representation");
                    Console.WriteLine("{0,76}", "of inputfile's bytes to outputfile.");
                    Console.WriteLine("{0,74}", "The optional parameter linelength");
                    Console.WriteLine("{0,77}", "determines every how many characters");
                    Console.WriteLine("{0,60}", "there's a new line.");

                    Console.ReadKey(true);

                    return; // We won't be using multiple parameters in this application, so stop scanning for more when one is found
                }
            }
        }
    }
}
